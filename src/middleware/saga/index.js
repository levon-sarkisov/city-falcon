  
import { put, call, takeEvery, all } from "redux-saga/effects";
import * as types from "../../const";
import { REQUEST, SUCCESS } from "../../const/requests";
import * as services from "../../services/api";

function* getConferences(data) {
  const payload = yield call(() => services.getStories(data.payload));
  yield put({ type: types.STORIES[SUCCESS], payload });
}

function* startToFetchStories(data) {
  yield takeEvery(
    types.STORIES[REQUEST], getConferences);
}

export default function* rootSaga() {
  yield all([
    startToFetchStories(),
  ]);
}
import { API_URL } from '../const/api';

function callApi(endpoint, header) {
  return fetch(`${API_URL}${endpoint}`, header)
  .then(response => response.json())
  .catch(error => console.log('ERROR MESSAGE: ' + error));
}

export const getStories = body => callApi('stories.json', {
  method: 'get',
});

import React, { useState } from "react";
import { Collapse } from 'react-bootstrap';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBook } from "@fortawesome/free-solid-svg-icons";
import { faTwitter } from "@fortawesome/free-brands-svg-icons";
import Social from '../social';

export default function Story(props) {
  const [open, setOpen] = useState(false);
  const { data } = props;

  return (
    <div className="story">
      <div className="score">
        <span className="score__value">{data.score}%</span>
        <span
            onClick={() => setOpen(!open)}
            className={`button-toggle${open ? ' button-toggle__active' : ''}`}
            aria-expanded={open}
          />
      </div>
      <dl>
        <dt className="story__title">{data.title}</dt>
        <dd className="story__image" style={{ backgroundImage: `url(${data.author_image_url})` }} />
        <dd>
          <div className="story__time">
            <span className="story__type">
              {data.type === 'Tweet' ? <FontAwesomeIcon icon={faTwitter} /> : <FontAwesomeIcon icon={faBook} />}
            </span> {data.publishTime}
          </div>
        </dd>
      </dl>
      <Collapse in={open}>
        <div className="collaps__wrapper">
          <div className="story__description">{data.description}</div>
          <Social />
        </div>
      </Collapse>
    </div>
  );
}

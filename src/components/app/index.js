import React from "react";
import { Container } from 'react-bootstrap';
import Header from "../header";
import StoriesList from '../stories-list';

export default function App(props) {
  return (
    <Container>
      <Header />
      <StoriesList />
    </Container>
  );
}

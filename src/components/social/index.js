import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBookmark, faThumbsUp, faThumbsDown } from "@fortawesome/free-solid-svg-icons";

export default function Social(props) {

  return (
    <div className="social">
      <span className="social__item">
        <FontAwesomeIcon icon={faBookmark} />
        <span className="social__name">Bookmark</span>
      </span>
      <span className="social__item">
        <FontAwesomeIcon icon={faThumbsUp} />
        <span className="social__name">Like</span>
      </span>
      <span className="social__item">
        <FontAwesomeIcon icon={faThumbsDown} />
        <span className="social__name">Dislike</span>
      </span>
    </div>
  );
}

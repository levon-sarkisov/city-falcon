import React from "react";
import { Dropdown } from 'react-bootstrap';
import logo from "../../assets/img/logo.gif";
import avatar from "../../assets/img/avatar.gif";

export default function Header() {

  return (
    <div className="header">
      <h1><img src={logo} alt="City Falcon" /></h1>
      <dl className="user">
        <dt><img src={avatar} alt="John Doe" /></dt>
        <dd>
          <Dropdown alignRight>
            <Dropdown.Toggle className="user__button">
              John Doe
            </Dropdown.Toggle>

            <Dropdown.Menu>
              <Dropdown.Item href="#/test-1">Text 1</Dropdown.Item>
              <Dropdown.Item href="#/test-2">Text 2</Dropdown.Item>
              <Dropdown.Item href="#/test-3">Text 3</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </dd>
      </dl>
    </div>
  );
}

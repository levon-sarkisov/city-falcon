import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getStoriesAction } from "../../actions/stories";
import Story from '../story';

export default function StoriesList(props) {
  const { all } = useSelector(state => state.stories);
  const dispatch = useDispatch();

  useEffect(() => {
    if (!all) {
      dispatch(getStoriesAction.request());
    }
  }, [dispatch, all]);

  return (
    <div className="story-list">
      <h2>Watchlist</h2>
      {all && all.stories.map(story => <Story key={story.id} data={story} />)}
    </div>
  );
}

import {
  STORIES,
} from '../../const';
import { SUCCESS, FAILURE } from "../../const/requests";

const stories = (state = {}, action) => {
  switch (action.type) {
    case STORIES[SUCCESS]:
      return Object.assign(
        {}, state, {
          all: action.payload,
        },
      );
    case STORIES[FAILURE]:
      return Object.assign(
        {}, state, {
          all: null,
        },
      );
    default:
      return Object.assign(
        {}, state,
      );
  }
};

export default stories;

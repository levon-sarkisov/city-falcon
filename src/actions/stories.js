import * as type from "../const";
import { REQUEST, SUCCESS, FAILURE } from "../const/requests";

function action(type, payload = {}) {
  return { type, ...payload };
}

export const getStoriesAction = {
  request: () => action(type.STORIES[REQUEST]),
  success: data => action(type.STORIES[SUCCESS], data),
  failure: error => action(type.STORIES[FAILURE], error)
};
